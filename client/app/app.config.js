// Defines client-side routing
(function () {
    angular
        .module("Grocery")
        .config(RouteConfig)
    RouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function RouteConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: './app/search/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'ctrl'
            })
            
            .state('editWithParam', {
                url: '/edit/:upc12',
                templateUrl: './app/edit/edit.html',
                controller: 'EditCtrl',
                controllerAs: 'ctrl'
            })


        $urlRouterProvider.otherwise("/search");
    }
})();