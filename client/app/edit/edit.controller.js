(function () {
    'use strict';
    angular
        .module("Grocery")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "$stateParams", "GroceryService"];

    function EditCtrl($filter, $stateParams, GroceryService) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a department object. We expose the department object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.dept_no = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
       
        vm.initDetails = initDetails;
        vm.search = search;
        vm.toggleEditor = toggleEditor;
        vm.updateDeptName = updateDeptName;


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();
        console.log("before state");
        if($stateParams.upc12){
            console.log("$stateparams " + $stateParams.upc12);
            vm.upc12 = $stateParams.upc12;
            vm.search();
        }


        // Function declaration and definition -------------------------------------------------------------------------
        

        // Initializes department details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.upc12 = "";
            vm.result.brand = "";
            vm.result.name = "";
            
            vm.showDetails = false;
            vm.isEditorOn = false;
        }

        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
//            initDetails();
            vm.showDetails = true;

            GroceryService
                .retrieveGroceryByID(vm.upc12)
                .then(function (result) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    // Exit .then() if result data is empty
                    if (!result.data)
                        return;

                    // The result is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                   
                
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        }

        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

        // Saves edited upc12
        function updateGrocery() {
            console.log("-- show.controller.js > save()");
            GroceryService
                .updateGrocery(vm.upc12, vm.brand, vm.name)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

        
    }
})();