// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("Grocery")
        .service("GroceryService", GroceryService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    GroceryService.$inject = ['$http'];

    // DeptService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function GroceryService($http) {

        // Declares the var service and assigns it the object this (in this case, the DeptService). Any function or
        // variable that you attach to service will be exposed to callers of DeptService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
    
        service.retrieveGrocery = retrieveGrocery;
        service.updateGrocery = updateGrocery;
        


        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------

        // retrieveDept retrieves department information from the server via HTTP GET.
        // Parameters: None. Returns: Promise object
        function retrieveGrocery(searchString) {
            return $http({
                method: 'GET',
                url: 'api/grocery',
                params: {'searchString': searchString}
            });
        }

       // updateDept uses HTTP PUT to update department name saved in DB; passes information as route parameters and via
        // HTTP HEADER BODY IMPORTANT! Route parameters are not the same as query strings!
        function updateGrocery(upc12, brand, name) {
            return $http({
                method: 'PUT'
                , url: 'api/grocery/' + upc12
                , data: {
                    upc12: upc12,
                    brand: brand,
                    name: name
                }
            });
        }
       

         function retrieveGroceryByID(upc12) {
            return $http({
                method: 'GET'
                , url: "api/grocery/" + upc12
            });
        }
    }
})();